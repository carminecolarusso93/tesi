package servizioAssociazioneLinkArea;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import util.SettingReader;
/**
 * La classe GeneratorThread.java ha il compito di associare il link alla giusta area e di inviare
 * i sample al corretto nodo area in base al link specificato nel sample. Per l'associazione
 * tra link ed area si sfruttano le informazioni presenti all'interno del file "Links.csv" o di un suo sottoinsieme mentre il 
 * file contenete i samples � "Observation.csv" o di un suo sottoinsieme.
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public class GeneratorThread extends Thread{

	//si utilizza un fattore di scala per l'invio del messaggio. Si considera 
	//il valore qui presente � solo indicativo, dato che verr� poi sostituito dal valore presente nel file setting.xml
	private int scala=10;
	private String url="tcp://localhost:61616";
	//HashMap tenere traccia dell'associazione tra linkId ed areaName
	private  HashMap<String, String> associazioni=new HashMap<>();
	//hashMap utilizzata per aevere una JMSQueue per ogni area name
	private HashMap<String, Queue> JMSQueues=new HashMap<>();
	private  String linkFile;
	private  String obsFile;


	/**
	 * Costruttore del thread a cui si passano due valori:
	 * @param linkFile il nome del file contenente le strade e le aree su cui il thread dovr� lavorare.
	 * @param obsFile il nome del file contenente i sample riferenti solo alle sue aree e strade.
	 */
	public GeneratorThread(String linkFile, String obsFile) {

		this.linkFile=linkFile;
		this.obsFile=obsFile;
	}

	
	/**
	 * Il metodo run contiene le operazioni eseguite dal thread:
	 * Legge la scala da voler utilizzare e crea una sessione con la quale creer� tante code quante sono le aree presenti nel suo 
	 * file linkFile.
	 * Esegue il riempimento delle code in base a i sample letti nel file con nome presente nella variabile obsFile
	 */
	public void run() {
		SettingReader st = new SettingReader();

		// righe di codice che servono per cambiare la variabile scala in base al valore presente nel file xml "settings.xml"
		String value = st.readElementFromFileXml("settings.xml", "generator", "scala");
		scala = Integer.parseInt(value);
		System.out.println("scala = " + scala);

		url = st.readElementFromFileXml("settings.xml", "areaNode", "urlIn");


		QueueConnectionFactory factory=new ActiveMQConnectionFactory(url);
		QueueConnection connection = null;
		QueueSession session = null;

		try {
			connection = factory.createQueueConnection();
			session=connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

		} catch (JMSException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*
		 * Questo metodo associa il link alla specifica area e crea una JMSQueue per ogni area
		 * 
		 */
		associaLinkArea(session);
		/*
		 * Questo metodo, a partire dai sample contenuti nel file, invia il messaggio alla coda
		 * in base all'informazione relativa al link presente nel file
		 * 
		 */
		try {
			sendMessage(session);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Questo metodo legge una linea dal file contenete i sample (ogni linea del file � un sample),
	 * determina la giusta queue alla quale inviare il sample in base all'area cui appartiene il link sul quale il veicolo transita,
	 * costruisce il messaggio JMS e lo invia. Il messaggio JMS � un TextMessage. Si setta come testo del messaggio l'intera stringa costituente il sample
	 * e si definiscono le seguenti properties:
	 * "link": un long che rappresenta l'id del link
	 * "coverage": un float che indica la copertura della strada
	 * "timestamp": una stringa che rappresenta il tempo in cui � stata effettuata l'osservazione
	 * "speed": un float che rappresenta la velocit�
	 * L'invio dei messaggi � temporizzato. Si considera l'intervallo di tempo che intercorre tra due samples successivi
	 * e si divide per una fattore di scala modificabile nel file setting.xml. 
	 * @param QueueSession session la sessione con la quale verr� creato il sender
	 * @throws ParseException 
	 * @throws InterruptedException
	 * @throws JMSException
	 */
	private  void sendMessage(QueueSession session) throws InterruptedException {
		// TODO Auto-generated method stub

		//SettingReader st = new SettingReader();
		//String fileName = st.readElementFromFileXml("settings.xml", "Files", "observations");
		File observationFile=new File(obsFile);
		Scanner sc2;
		String tmpPrecedente=null;
		String tmpAttuale=null;
		int i=0;

		try {
			sc2=new Scanner(observationFile);
			while(sc2.hasNextLine())
			{
				String obs=sc2.nextLine();
				String obsArray[]=obs.split(";");

				//timestamp attuale
				tmpAttuale=obsArray[3];

				//se il timestamp � il primo, tempo attuale e tempo precedente coincidono 
				if(i==0)
				{
					tmpPrecedente=tmpAttuale;
				}

				//calcolo differenza tra i timestamp di due sample successivi
				long millisDiff=diffTempi(tmpPrecedente , tmpAttuale);

				//utilizzato per inviare i messaggi in modo da rispettare
				//l'intervallo di tempo che intercorre tra due messaggi in base ai timestamp
				Thread.sleep(millisDiff);
				i++;
				tmpPrecedente=tmpAttuale;
				Queue queueLink=JMSQueues.get(associazioni.get(obsArray[1]));
				QueueSender sender;
				try {
					sender = session.createSender(queueLink);
					//Messaggio JMS ti tipo testuale
					//si setta come testo del messaggio l'intera linea del file
					Message message=session.createTextMessage(obs);
					//propriet� del messaggio

					message.setLongProperty("link", Long.parseLong(obsArray[1]));
					obsArray[2]=obsArray[2].replace(',' , '.');
					message.setFloatProperty("coverage", Float.parseFloat(obsArray[2]));
					message.setStringProperty("timeStamp", obsArray[3]);
					message.setFloatProperty("speed", Float.parseFloat(obsArray[4]));
					//invio del messaggio
					sender.send(message);
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					System.err.println("ERRORE JMS: " + e);
				}

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Questo metodo effettua l'associazione tra link ed area. Si ottiene una hashMap con chiave di tipo Stringa e valore di tipo Stringa in cui
	 * la chiave sar� il linkId ed il valore l'areaName. Inoltre crea una JMSQueue per ogni area. Si ottiene una HashMap la cui chiave di tipo stringa 
	 * rappresenta il nome dell'area ed il cui valore di tipo JMSQueue rappresenta la coda associata.
	 * @param Queuesession session la sessione con la quale si creer� la queue
	 * @throws JMSException
	 */
	private  void associaLinkArea(QueueSession session) {
		// TODO Auto-generated method stub

		//SettingReader st = new SettingReader();
		//String fileName = st.readElementFromFileXml("settings.xml", "Files", "links");
		File linksFile = new File(linkFile); 
		Queue queue=null;
		Scanner sc;

		try {
			sc = new Scanner(linksFile);
			while (sc.hasNextLine()) 

			{


				String s= sc.nextLine();	
				String l[] = s.split(";");

				associazioni.put(l[0], l[8]);
				try {
					queue=session.createQueue(l[8]);
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					System.err.println("ERRORE JMS: " + e);
				}
				JMSQueues.put(l[8], queue);

			}


		}catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}


	/**
	 * Questo metodo serve a calcolare la differenza tra i tempi di ricezione dei due sample. Il primo parametro 
	 * indica il tempo del primo campione, il secondo indica il tempo di ricezione del campione successivo
	 * @param String d1 tempo del primo campione
	 * @param String d2 tempo del secondo campione
	 * @return long millisDiff/scala differenza dei tempi dei due campioni in millisecondi diviso la scala specificata in settings.xml
	 * @throws ParseException
	 */
	private  long diffTempi(String d1, String d2) 
	{
		SimpleDateFormat fmt=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date1 = null;
		try {
			date1 = fmt.parse(d1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
		}
		Date date2 = null;
		try {
			date2 = fmt.parse(d2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.err.println("ERRORE PARSING DATA: " + e);

		}

		long millisDiff=date2.getTime() - date1.getTime();
		return millisDiff/scala;
	}

	/**
	 * Main il cui compito eseguire un numero di thread par al valore presente in setting.xml.
	 * Ad ogni thread passa due stringhe che rappresentano i nomi del file da leggere.
	 * I nomi partono da 0 a numero di thread-1 e devono essere inseriti in due cartelle, link e obs. 
	 * @param args
	 */
	public static void main(String[] args) {
		SettingReader st = new SettingReader();

		// righe di codice che servono per cambiare la variabile scala in base al valore presente nel file xml "settings.xml"
		String value = st.readElementFromFileXml("settings.xml", "generator", "numberOfThreads");
		int threads = Integer.parseInt(value);
		System.out.println("threads = " + threads);
		
		GeneratorThread[] generators= new GeneratorThread[threads];
		for(int i=0; i<threads;i++) {
			generators[i]=new GeneratorThread("links/"+i+".csv","obs/"+i+".csv");
		}
		for(int i=0; i<threads;i++) {
			generators[i].start();
		}
	}
	
}
