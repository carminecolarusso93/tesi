package util;

import java.io.Serializable;

public abstract class Payload implements Serializable{
	private static final long serialVersionUID = 7826412956628173147L;
	
	protected String payloadId;
	protected Integer roadLaneId;
	
	public Integer getRoadLaneId() { return roadLaneId; }
	public void setRoadLaneId(Integer roadLaneId) { this.roadLaneId = roadLaneId; }
	public String getPayloadId() { return payloadId; }
	public void setPayloadId(String payloadId) { this.payloadId = payloadId; }

}
