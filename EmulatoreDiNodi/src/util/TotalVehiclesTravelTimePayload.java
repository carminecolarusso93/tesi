package util;
import java.util.Date;

import com.google.gson.Gson;

public class TotalVehiclesTravelTimePayload extends Payload {
	private static final long serialVersionUID = -3202089435452432074L;
	
	private double travelTime;
	private int numVehicles;
	private Date startTime, endTime;
	
	public double getTravelTime() { return travelTime; }
	public void setTravelTime(double travelTime) { this.travelTime = travelTime; }
	public int getNumVehicles() { return numVehicles; }
	public void setNumVehicles(int numVehicles) { this.numVehicles = numVehicles; }
	public Date getStartTime() { return startTime; }
	public void setStartTime(Date startTime) { this.startTime = startTime; }
	public Date getEndTime() { return endTime; }
	public void setEndTime(Date endTime) { this.endTime = endTime; }

	public TotalVehiclesTravelTimePayload(String payloadId, double travelTime, int numVehicles, Date startTime, Date endTime, Integer roadLaneId) {
		super.setPayloadId(payloadId);
		this.travelTime = travelTime;
		this.numVehicles = numVehicles;
		this.startTime = startTime;
		this.endTime = endTime;
		super.setRoadLaneId(roadLaneId);
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
	public static void main(String[] args) {
		TotalVehiclesTravelTimePayload t = new TotalVehiclesTravelTimePayload("TotalVehiclesTravelTimePayload", 1d, 5, new Date(), new Date(), new Integer(0));
		System.out.print(t);
	}

}
