package util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

public class Packet implements Serializable{
	private static final long serialVersionUID = 3930024432958038982L;
	
	public static final HashMap<String, Class<?>> payloadsMap;
	static {
		payloadsMap = new HashMap<String, Class<?>>();
		
		payloadsMap.put("TotalVehiclesTravelTimePayload", TotalVehiclesTravelTimePayload.class);
		payloadsMap.put("TravelTimeResults", TotalVehiclesTravelTimePayload.class);
	}


	protected long edgeId;
	protected long timestamp;
	protected ArrayList<Payload> payloads;
	
	public long getEdgeId() { return edgeId; }
	public void setEdgeId(int edgeId) { this.edgeId = edgeId; }
	public long getTimestamp() { return timestamp; }
	public void setTimestamp(long timestamp) { this.timestamp = timestamp; }
	public ArrayList<Payload> getPayloads() { return payloads; }
	public void setPayloads(ArrayList<Payload> payloads) { this.payloads = payloads; }
	
	public Packet() {}

	public Packet(long edgeId, ArrayList<Payload> payloads) {
		this.edgeId = edgeId;
		this.timestamp = new Date().getTime();
		this.payloads = payloads;
	}
	
	public Packet(long edgeId, Payload payload) {
		this.edgeId = edgeId;
		this.timestamp = new Date().getTime();
		this.payloads = new ArrayList<Payload>(); 
		this.payloads.add(payload);
	}
	
	public Packet(long edgeId, ArrayList<Payload> payloads, long timestamp) {
		this.edgeId = edgeId;
		this.timestamp = timestamp;
		this.payloads = payloads;
	}
	
	public Packet(long edgeId, Payload payload, long timestamp) {
		this.edgeId = edgeId;
		this.timestamp = timestamp;
		this.payloads = new ArrayList<Payload>(); 
		this.payloads.add(payload);
	}
	
	public static String toJson(Object o) {
		return new Gson().toJson(o);		
	}
	
	
	public static Packet putTimestamp(Packet packet) {
		if(packet.timestamp <= 0) packet.setTimestamp(new Date().getTime());
		return packet;
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
	public static void main(String[] args) {
		/*
	Packet p = new Packet(0, pl);
		String json = Packet.toJson(p);
		System.out.println(json);
		Packet p2 = (Packet) Packet.fromJson(json);
		System.out.println(p2);
		 */
	
	}
	
}
