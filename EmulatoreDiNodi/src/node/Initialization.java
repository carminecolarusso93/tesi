package node;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import link.Link;
import util.SettingReader;


/**
 * Classe utilizzata per l'inizializzazione dei vari AreaNode. Il processo parte dalla lettura da file delle informazioni
 * dei singoli link. Da questi si recuperano il numero di aree e di conseguenza si instanziano i diversi AreaNode, contestualmente vengono generati
 * tutti i link.
 * 
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public class Initialization {

	/**
	 * 
	 * Enum utilizzato per identificare i due tipi di nodo sensore realizzabili.
	 * AVGSPEEDONLINK identifica il sensore che calcola la velocit� media su un link in un intervallo di tempo
	 * AVGSPEEDVEHICLEONLINK identifica il sensore che calcola la velocit� media di un veicolo su un link
	 *
	 */
	private enum SensorType {
		AVGSPEEDONLINK,
		AVGSPEEDVEHICLEONLINK
	}

	//private static String urlIn = "tcp://localhost:61616";
	//private static String urlOut = "tcp://localhost:61616";
	private static String linkFilePath="Links.csv";

	
	public static void main(String[] args) {

		int intSt;
		SensorType sensorType = SensorType.AVGSPEEDONLINK;
		
		
		// righe di codice che servono per cambiare la variabile sensorType in base al valore presente nel file xml "settings.xml"
		SettingReader st = new SettingReader();
		String value = st.readElementFromFileXml("settings.xml", "areaNode", "sensorType");
		System.out.println(value);		
		if (Integer.parseInt(value) == 0 )
		{
			sensorType = SensorType.AVGSPEEDONLINK;
		} else {
			sensorType = SensorType.AVGSPEEDVEHICLEONLINK;
		}
		System.out.println(sensorType);
								
		String multipleNorthBoundQueues;
		boolean boolMultipleNorthBoundQueues=false;
		value = st.readElementFromFileXml("settings.xml", "areaNode", "multipleNorthBoundQueues");
		//System.out.println(value);		
		boolMultipleNorthBoundQueues = Integer.parseInt(value) == 1 ;
		//System.out.println(boolMultipleNorthBoundQueues);
		
		String urlIn = st.readElementFromFileXml("settings.xml", "areaNode", "urlIn");
		String urlOut = st.readElementFromFileXml("settings.xml", "areaNode", "urlOut");
		
		String intervallo = st.readElementFromFileXml("settings.xml", "Link", "intervallo");
		String startTime = st.readElementFromFileXml("settings.xml", "Link", "startTime");
		
		//boolean syntheticMessage=true;
		int msgType=-1;
		value = st.readElementFromFileXml("settings.xml", "Link", "msgType");
		//System.out.println(value);		
		msgType = Integer.parseInt(value) ;
		
		linkFilePath = st.readElementFromFileXml("settings.xml", "Files", "links");
		System.out.println(linkFilePath);
		
		HashMap<String, AreaNode> areas = new HashMap<String, AreaNode>();

		File file =  new File(linkFilePath); 
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()){


				String s= sc.nextLine();	
				String l[] = s.split(";");
				AreaNode n = null;
				//verifico che non sia la prima riga del file dove vengono specificati i campi del dataset
				if(!l[0].equals("id")) {
					//effettuo un replace nella stringa altrimenti non e' possibile parsare il float
					l[1]=l[1].replace(',' , '.');
					//creazione del link
					Link link =new Link(Long.parseLong(l[0]),Float.parseFloat(l[1]),Integer.parseInt(l[2]),Integer.parseInt(l[3]),
								        Integer.parseInt(l[4]),Integer.parseInt(l[5]),
							            Integer.parseInt(l[6]),l[7],l[8],l[9],l[10],
									    Integer.parseInt(intervallo),
									    startTime,msgType
									    );
					//verifico che l'areaNode associato a quell'area non sia stata gia' creato
					if (areas.containsKey(l[8])) {
						//se l'area node esiste gia' aggiungo il link all'areaNode
						areas.get(l[8]).addLink(link);
					}
					else{
						//se l'area node non e' stato ancora crea l'area node basandomi sul tipo di nodo sensore da creare
						if(sensorType==SensorType.AVGSPEEDONLINK) {
							n = new AvgSpeedLink(urlIn,urlOut,l[8],link,boolMultipleNorthBoundQueues);
						}else if(sensorType==SensorType.AVGSPEEDVEHICLEONLINK) {
							n = new AvgSpeedVehicleLink(urlIn,urlOut,l[8],link,boolMultipleNorthBoundQueues);
						}
						//aggiungo l'areaNode appena creata all'HashMap delle aree
						areas.put(l[8], n);
					}
				}
			}

		} catch (FileNotFoundException e) {
			System.err.println("File non disponibile "+ e);
		} finally {
			sc.close();
		}


		System.out.println("Inizializzazione Terminata");
		//utilizzo un busy waiting per tenere attive le varie istanze di AreaNode create
		while (true) {
		}
	}

}
