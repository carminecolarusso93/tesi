package node;

import java.util.HashMap;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import link.Link;

/**
 * La classe viene utilizzata per estendere l'AreaNode definendone il comportamento. Viene costruito un sensore in grado di calcolare la 
 * velocit� media di un veicolo su un link.
 *
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public class AvgSpeedVehicleLink extends AreaNode{

	/**
	 * Questo costruttore prevede il passaggio di una hashMap di links gi� riempita, in modo da consentire un'inizializzazione esterna.
	 * Gli altri parametri servono a gestire la connessione attraverso l'url verso il broker jms,
	 * il nome dell'area sottoforma di stringa,
	 * e un booleano per decidere se il nodo inoltrer� i sample elaborati e arricchiti verso una sola coda di northBound oppure se ne inizializzer� una per ogni area.
	 * @param urlIn Stringa utilizzata per completare la connessione al broker jms in entrata
	 * @param urlOut Stringa utilizzata per completare la connessione al broker jms in uscita
	 * @param areaName Stringa che identifica il nome dell'area
	 * @param links	HashMap che lega i link al loro identificativo
	 * @param multipleQueues Valore booleano utilizzato per decidere se utilizzare una singola coda d'uscita al northBound per tutte le aree oppure una per area
	 */
	public AvgSpeedVehicleLink(String urlIn, String urlOut, String areaName, HashMap<Long, Link> links, boolean multipleQueues) {
		super(urlIn, urlOut,areaName,links, multipleQueues);
	}
	
	/**
	 * Costruttore utilizzato per la creazione del sensore, prevede il passaggio del singolo link al posto dell'intera hashmap gi� inizializzata, questo consente di creare una nuova area semplicemente scorrendo i vari link.
	 * Gli altri parametri sono uguali al precedente costruttore.  
	 * @param urlIn Stringa utilizzata per completare la connessione al broker jms in entrata
	 * @param urlOut Stringa utilizzata per completare la connessione al broker jms in uscita
	 * @param areaName Stringa che identifica il nome dell'area
	 * @param link Il primo link da aggiungere all'area
	 * @param multipleQueues Valore booleano utilizzato per decidere se utilizzare una singola coda d'uscita al northBound per tutte le aree oppure una per area
	 */
	public AvgSpeedVehicleLink(String urlIn, String urlOut, String areaName, Link link, boolean multipleQueues) {
		super(urlIn, urlOut,areaName,link, multipleQueues);
	}
	
	@Override
	protected MessageListener CreateMessageListener() {
		return new MessageListener() {
			public void onMessage(Message msg) {
				try {
					//questo codice verra' eseguito ogni volta che viene aggiunto un messaggio sulla
					//coda corrispondete all'area
					long link=msg.getLongProperty("link");
					String l[]=getLinks().get(link).singleSampleTravelTime (msg.getFloatProperty("speed"),msg.getFloatProperty("coverage"),msg.getStringProperty("timeStamp"));
					SendMessage(l[0],msg.getLongProperty("link"), msg.getStringProperty("timeStamp"), msg.getFloatProperty("speed"), l[1]);
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
	}
}
