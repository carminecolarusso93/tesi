package node;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

import link.Link;

/**
 * La classe serve a modellare il nodo responsabile di gestire un'intera area geografia.
 *
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public abstract class AreaNode {

	private String areaName;
	private HashMap<Long, Link> links= new HashMap<Long, Link>();
	private QueueReceiver qr;
	private QueueSession sessionIn;
	private QueueSession sessionOut;
	private MessageProducer mp;
	private AreaNode myInstance = this;
	private MessageListener myListener;
	private boolean multipleNorthBoundQueues;
	private String urlIn;
	private String urlOut;

	/**
	 * Questo costruttore prevede il passaggio di una hashMap di links gi� riempita, in modo da consentire un'inizializzazione esterna.
	 * Gli altri parametri servono a gestire la connessione attraverso l'url verso il broker jms,
	 * il nome dell'area sottoforma di stringa,
	 * e un booleano per decidere se il nodo inoltrera' i sample elaborati e arricchiti verso una sola coda di northBound oppure se ne inizializzera' una per ogni area.
	 * @param urlIn Stringa utilizzata per completare la connessione al broker jms in entrata
	 * @param urlOut Stringa utilizzata per completare la connessione al broker jms in uscita
	 * @param areaName Stringa che identifica il nome dell'area
	 * @param links	HashMap che lega i link al loro identificativo
	 * @param multipleQueues Valore booleano utilizzato per decidere se utilizzare una singola coda d'uscita al northBound per tutte le aree oppure una per area
	 */
	public AreaNode(String urlIn, String urlOut, String areaName, HashMap<Long, Link> links, boolean multipleQueues) {
		this(urlIn, urlOut,areaName, multipleQueues);
		this.setLinks(links);
	}
	
	
	/**
	 * Questo costruttore prevede il passaggio del singolo link al posto dell'hashmap.
	 * Gli altri parametri sono uguali al precedente costruttore.
     * @param urlIn Stringa utilizzata per completare la connessione al broker jms in entrata
	 * @param urlOut Stringa utilizzata per completare la connessione al broker jms in uscita
	 * @param areaName Stringa che identifica il nome dell'area
	 * @param link Il primo link da aggiungere all'area
	 * @param multipleQueues Valore booleano utilizzato per decidere se utilizzare una singola coda d'uscita al northBound per tutte le aree oppure una per area
	 */
	public AreaNode(String urlIn, String urlOut, String areaName, Link Link, boolean multipleQueues) {
		this(urlIn, urlOut,areaName, multipleQueues);
		getLinks().put(Link.getId(),Link);
	}
	
	/**
	 * Costruttore privato che mette a fattor comune le operazioni obbligatorie da fare durate la creazione di un AreaNode, in particolare la gestione delle connessioni, l'aggiornamento del nome dell'area
	 * e l'avvio dei metodi responsabili del collegamento con il broker jms.
	 * @param urlIn Stringa utilizzata per completare la connessione al broker jms in entrata
	 * @param urlOut Stringa utilizzata per completare la connessione al broker jms in uscita
	 * @param areaName Stringa che identifica il nome dell'area
	 * @param multipleQueues Valore booleano utilizzato per decidere se utilizzare una singola coda d'uscita al northBound per tutte le aree oppure una per area
	 */
	private AreaNode(String urlIn, String urlOut, String areaName, boolean multipleQueues) {
		this.areaName = areaName;
		this.urlIn=urlIn;
		this.urlOut=urlOut;
		multipleNorthBoundQueues=multipleQueues;
		QueueConnections();
		setMyListener(CreateMessageListener());
		setQueueListener();
	}
	
	/**
	 * Metodo utilizzato per aggiungere un link all'hashMap links, che contiene tutti i link dell'area
	 * @param l Link da aggiungere all'area
	 */
	public void addLink(Link l) {
		getLinks().put(l.getId(),l);
	}
	
	/**
	 * Metodo che restituisce il numero di link presenti nell'hashMap e quindi nell'area
	 * @return int il numero di link attualmente memorizzat come intero
	 */
	public int getNumberOfLinks() {
		return getLinks().size();
	}

	/**
	 * Metodo publico utilizzato per esportare l'hashMap che lega il valore linkId alla rispettiva istanza.
	 * @return HashMap L'hashMap dei link
	 */
	public HashMap<Long, Link> getLinks() {
		return links;
	}

	/**
	 *  Metodo publico utilizzato per importare l'hashMap che lega il valore linkId alla rispettiva istanza.
	 * @param links L'hashMap dei link
	 */
	public void setLinks(HashMap<Long, Link> links) {
		this.links = links;
	}

	/**
	 * Metodo pubblico utilizzato per esportare l'istanza dell'ArenaNode in questione.
	 * @return AreaNode L'istanza di AreaNode che lo invoca
	 */
	public AreaNode getAreaNodeInstance() {
		return myInstance;
	}

	/**
	 * Metodo privato utilizzato per settare il listener che caratterizza il tipo di azione da fare alla ricezione di un messaggio.
	 * @param myListener Il messageListener che utilizzer� questo AreaNode
	 */
	private void setMyListener(MessageListener myListener) {
		this.myListener = myListener;
	}
	
	/**
	 * Metodo privato responsabile dell'attivazione della connessione verso il broker Jms. Viene chiamato dal costruttore e si occupa di settare le variabili di sessione per la creazione dei messaggi e il messageProducer
	 * responsabile dell'invio degli stessi.
	 */
	private void QueueConnections() {
		//String url = "tcp://localhost:61616";
		QueueConnectionFactory factoryIn = new ActiveMQConnectionFactory(urlIn);
		QueueConnectionFactory factoryOut = new ActiveMQConnectionFactory(urlOut);
	//	QueueConnectionFactory factoryOut = new ActiveMQConnectionFactory("artemis","simetraehcapa",urlOut);
		try {
			QueueConnection connectionIn = factoryIn.createQueueConnection();
			sessionIn = connectionIn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue receiveQueue = sessionIn.createQueue(areaName);
			qr = sessionIn.createReceiver(receiveQueue);
			connectionIn.start();
			
			QueueConnection connectionOut = factoryOut.createQueueConnection();
			sessionOut = connectionOut.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			connectionOut.start();
			
			Queue sendQueue;
			if(multipleNorthBoundQueues) {
				sendQueue = sessionOut.createQueue(areaName+"-NorthBound");
			}else {
				sendQueue = sessionOut.createQueue("NorthBound");
			}
			mp = sessionOut.createProducer(sendQueue);
		} catch (JMSException e) {
			System.err.println("Errore JMS: " + e);
		}
		;
	}
	
	/**
	 * Metodo publico utilizzato dagli oggetti link per sollecitare l'areaNode alla creazione e all'invio del messaggio jms verso la coda del northBound.
	 * (La coda in questione potrebbe essere una comune a tutti gli AreaNode oppure relativo alla singola area, questo viene gestito all'atto della creazione dell'areaNode)
	 * I parametri passati servono a creare il messaggio  
	 * @param messageBody Il corpo del messaggio sottoforma di testo,
	 * @param linkId Il long che identifica la strada,
	 * @param timestamp Il timestamp di rifermento dell'osservazione,
	 * @param speed	La velocita' media con la quale si � attraversata la strada nell'intervallo di tempo,
	 * @param travelTime Il tempo di percorrenza medio di quella strada nell'intervallo di tempo.
	 */
	public void SendMessage(String messageBody,long linkId, String timestamp, double speed, String travelTime){
		TextMessage msg;
		try {
			msg = sessionOut.createTextMessage();
			msg.setLongProperty("link", linkId);
			msg.setStringProperty("interval", timestamp);
			msg.setDoubleProperty("avgSpeed", speed);
			msg.setStringProperty("travelTime", travelTime);
			msg.setText(messageBody);
			mp.send(msg);
			//System.out.println("LindId " + linkId + " intervallo: " + timestamp + " media: " + speed + " travel time: " + travelTime);
			//System.out.println(msg.toString());
			System.out.println(messageBody);
			
			
			 	FileWriter fileWriter = new FileWriter("output.txt",true);
			    PrintWriter printWriter = new PrintWriter(fileWriter);
			    printWriter.println(areaName+ ";"+System.currentTimeMillis()+";"+timestamp+ ";"+speed);
			    printWriter.close();
			  
			    
		} catch (JMSException e) {
			System.err.println("Errore JMS: " + e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 *  Metodo publico utilizzato dagli oggetti link per sollecitare l'areaNode alla creazione e all'invio del messaggio jms verso la coda del northBound.
	 * @param messageBody
	 * @param linkId
	 */
	public void SendMessageTT(String messageBody, long linkId , String timestamp, String travelTime){
		TextMessage msg;
		try {
			
			msg = sessionOut.createTextMessage();
			msg.setLongProperty("link", linkId);
			msg.setText(messageBody);
			mp.send(msg);
			
			System.out.println(messageBody);
			
			FileWriter fileWriter = new FileWriter("output.txt",true);
		    PrintWriter printWriter = new PrintWriter(fileWriter);
		    printWriter.println(areaName+ ";"+System.currentTimeMillis()+";"+timestamp+ ";" + travelTime);
		    printWriter.close();
		  
			
			  
			    
		} catch (JMSException e) {
			System.err.println("Errore JMS: " + e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * Metodo privato utilizzato per il settaggio del listener sulla coda del broker jms
	 */
	private void setQueueListener() {
		try {
			qr.setMessageListener(myListener);
		} catch (JMSException e) {
			System.err.println("Errore JMS: " + e);
		}
	}
	
	/**
	 * Metodo astratto che serve a caratterizzare il comportamento dell'AreaNode attraverso 
	 * la definizione del listener che si attester�sulla coda del broker jms.
	 * @return MessageListener Il listener che verra' memorizzato nell'istanza di AreaNode
	 */
	protected abstract MessageListener CreateMessageListener() ;
	
}
