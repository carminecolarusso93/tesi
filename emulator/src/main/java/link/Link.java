package link;

import node.AreaNode;
import org.json.simple.JSONObject;
import util.Packet;
import util.Payload;
import util.TotalVehiclesTravelTimePayload;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * La classe Link  una classe che serve per calcolare le medie di un singolo link
 * verr usata da AreaNode che avr un'array di questi oggetti, uno per ogni link che fa parte di quell'area
 *
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public class Link {
	private long id;
	private float length;
	private int ffs,speedlimit,frc, netclass, fow;
	private String routenumber,areaname,name;
	private double travelTime; //somma dei travelTime in un intervallo di tempo di cui si deve fare la media prima dell'invio
	private float[][] geom;
	private int intervallo; // l'intervallo di tempo (inMinuti) nel quale mandare la media
	private Calendar calendar; // calendar che serve per calcolare l'intervallo
	private double somma; // i valori di cui fare la media
	private int count; // conta i valori aggiunti per fare la media al momento oppurtuno
	private int msgType;

	/**
	 * costruttore vuoto che setta intervallo a 3, somma, count, travelTime a 0 e la data da dove cominciare a calcolare gli intervalli a 06/09/2018 00:00:00 
	 */
	public Link() {
		intervallo = 3;				
		somma = 0;
		count = 0;				
		setCalendarFromString("06/09/2018 00:00:00");
		travelTime = 0;
		msgType=-1;
	}

	/**
	 * costruttore
	 * @param id identificativo arco
	 * @param length lunghezza in metri
	 * @param ffs Velocit in condizioni free-flow in Km/h
	 * @param speedlimit limite di velocit� della strada
	 * @param frc valore intero che descrive la natura della strada,
	 * @param netclass valore numerico che descrive l'importanza della strada 
	 * @param fow descrizione della forma della strada
	 * @param routenumber numero della strada
	 * @param areaname nome della zona
	 * @param name nome della strada
	 * @param geom serie di coppie (latitudine e longitudine) che indicano la geometria della strada
	 * @param intervallo intervallo di tempo del quale si vogliono le medie
	 * @param date la data da cui cominciare a calcolare gli intervalli
	 * @param msgType serve a definire se vogliamo il messaggio in uscita sintetico o no
	 */
	public Link(long id, float length, int ffs, int speedlimit, int frc, int netclass, int fow, String routenumber,
			String areaname, String name,String geom, int intervallo, String date, int msgType) {
		this.id = id;
		this.length = length;
		this.ffs = ffs;
		this.speedlimit = speedlimit;
		this.frc = frc;
		this.netclass = netclass;
		this.fow = fow;
		this.routenumber = routenumber;
		this.areaname = areaname;
		this.name = name;
		setGeomFromString(geom);
		this.intervallo = intervallo;
		this.msgType= msgType;
		somma = 0;
		count = 0;
		setCalendarFromString(date);
		travelTime = 0;
	}

	/**
	 * costruttore che imposta automaticamente intervallo a 3 e data da cui cominciare a "06/09/2018 00:00:00"
	 */
	public Link(long id, float length, int ffs, int speedlimit, int frc, int netclass, int fow,
			String routenumber,
			String areaname, String name, String geom,int msgType) {

		this(id, length,  ffs,  speedlimit,  frc,  netclass,  fow,  routenumber,
				areaname,  name, geom, 3, "06/09/2018 00:00:00",msgType);
	}

	/**
	 * Funzione che riceve i dati di cui fare la media di un singolo sample e li conserva aggiungendoli ai dati precedenti 
	 * per poi inviare una media nel momento in cui l'intervallo di tempo previsto passa
	 * le medie che calcola sono :
	 * la media delle velocit di una specifica strada in un certo intervallo di tempo
	 * la media del tempo trascorso per entrare ed uscire dalla specifica strada in un certo intervallo di tempo
	 * 
	 * @param receivedDate la data presente nel sample
	 * @param sampleSpeed la velocit presente nel sample che va aggiunta alla variabile somma utile per il calcolo della media quando l'intervallo sar passato
	 * @param coverage un numero compreso da 0 e 1 che rappresenta l'effettiva percentuale di strada percorda in quel sample rispetto al totale
	 * @param an on oggetto Areanode che sarerbbe il chiamante che serve per inviare la media una volta passato l'intervallo
	 */
	public void calculateAverage(Date receivedDate, float sampleSpeed, float coverage, AreaNode an) {
		while (receivedDate.compareTo(calendar.getTime()) > 0) { // vuol dire che la data  maggiore dell'intervallo e che quindi devo inviare la media
			Date date = calendar.getTime();  
			// DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String strDate = dateFormat.format(date);
			String messageBody;

			if(count == 0) {
				if(msgType == 1) {
					messageBody = this.generateSyntheticJsonBody(0,0, strDate);
					an.SendMessage(messageBody, this.id, strDate ,0,"00:00:00");
				}else if(msgType == 2){
					messageBody = this.generateJsonBody(0,0, strDate);
					an.SendMessage(messageBody, this.id , strDate,0,"00:00:00");
				}
				else if(msgType == 3) {
					try {
						messageBody = this.generateMessageAvgTravelTime(0, strDate);
						an.SendMessageTT(messageBody, this.id, strDate, "00:00:00");
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				//	a.SendMessage(messageBody, this.id  , strDate ,0,"00:00:00" );
			} else {
				if(msgType == 1) {
					messageBody = this.generateSyntheticJsonBody(somma/count, travelTime/count, strDate);
					String s = convertHoursInStringDate(travelTime/count);
					an.SendMessage(messageBody, this.id, strDate, somma/count, s);
				}else if(msgType == 2) {
					messageBody = this.generateJsonBody(somma/count, travelTime/count, strDate);
					String s = convertHoursInStringDate(travelTime/count);
					an.SendMessage(messageBody, this.id, strDate, somma/count, s);
				}
				else if(msgType == 3) {
					try {
						messageBody = this.generateMessageAvgTravelTime(travelTime/count, strDate);
						String s = convertHoursInStringDate(travelTime/count);
						an.SendMessageTT(messageBody, this.id, strDate, s);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				//String s = convertHoursInStringDate(travelTime/count);
				//	a.SendMessage(messageBody , this.id, strDate , somma/count, s );
			}
			/*
			if(count!=0)
				try {
					messageBody=this.generateMessageAvgTravelTime(travelTime/count, strDate);
					String s = convertHoursInStringDate(travelTime/count);
					a.SendMessage(messageBody , this.id, strDate , somma/count, s );
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 */
			somma = 0;
			count = 0;
			travelTime = 0;
			calendar.add(Calendar.MINUTE, intervallo);
		}
		somma = somma + sampleSpeed;
		// ogni volta che ricevo un sample calcolo in travel time di quel determinato sample e lo sommo
		travelTime = travelTime + ((coverage*length/1000)/sampleSpeed);
		count++;
	}


	/**
	 * Come calculateAverage(Date dataRicevuta, float valoreDaAggiungere,float coverage, AreaNode a)
	 * ma gli si passa una stringa che verr convertita in un oggetto date
	 * 
	 * 
	 * @param s la data presente nel sample in formato stringa
	 * @param speedSample
	 * @param coverage come in calculateAverage citato sopra
	 * @param an come in calculateAverage citato sopra
	 */
	public void calculateAverage(String s, float speedSample, float coverage, AreaNode an) {
		try {
			calculateAverage(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(s), speedSample, coverage, an);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * funzione che restituisce il tempo trascorso da quando un'autovettura  entrata in una strada a quando  uscita
	 * 
	 * @param sampleSpeed  la velocit di percorrenza su quella strada
	 * @param coverage un valore tra 0 e 1 che rappresenta la percentuale di strada percorsa
	 * @return un array che contiene come primo valore un equivalente al toString e come secondo il tempo trascorso per percorrere la strada
	 */
	public String[] singleSampleTravelTime (float sampleSpeed,float coverage,String timestamp) {
		String [] sArray = new String[2];
		String messageBody = null;
		if(msgType == 1)
			messageBody = this.generateSyntheticJsonBody(sampleSpeed, (coverage*length/1000)/sampleSpeed, timestamp);
		else if(msgType == 2)
			messageBody = this.generateJsonBody(sampleSpeed, (coverage*length/1000)/sampleSpeed, timestamp);
		sArray[0] = messageBody;
		sArray[1] = convertHoursInStringDate((coverage*length/1000)/sampleSpeed);
		return sArray;
	}

	//converte le ore trascorse in un oggetto date in una stringa
	/**
	 * in alcune funzioni devo restituire il tempo di percorrenza.
	 * dato che la variabile travelTime  un numero decimale che rappresenta le ore
	 * e voglio restituire una stringa in formato HH:mm:ss per renderla pi comprensibile
	 * viene usata questa funzione per fare la conversione
	 * 
	 * @param hoursToConvert le ore da convertire nel formato HH:mm:ss
	 * @return le ore convertite nel formato HH:mm:ss
	 */
	private String convertHoursInStringDate(double hoursToConvert) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.SECOND, (int) (hoursToConvert*3600));
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		return dateFormat.format(c.getTime());
	}

	// passo una stringa separata da | e creo un array 
	/**
	 * nei sample c' una lista di latitudini e longitudini che rappersentano la geometria della strada
	 * da questa stringa si vuole ottenere una matrice che contiene l'insieme di coppie
	 * @param s la stringa presente nel sample che rappresenta la geometria della stringa
	 */
	public void setGeomFromString(String s) {
		String[] l = s.split("\\|");
		geom = new float[l.length][2];
		for (int i = 0; i < l.length; i++) {	    	
			String[] n = l[i].split(",");
			for (int j = 0; j < n.length; j++)
				geom[i][j] = Float.parseFloat(n[j]);
		}
	}

	public float[][] getGeom() {
		return geom;
	}

	public void setGeom(float[][] geom) {
		this.geom = geom;
	}

	//come date ci vuole l'orario in cui l'intervallo comincia
	/**
	 * Setta l'inizio del giorno e delle ore in cui si deve andare a controllare l'intervallo di tempo in cui mandare le medie
	 * @param date l'inizio del giorno e delle ore come stringa in formato "dd/MM/yyyy HH:mm:ss"
	 */
	public void setCalendarFromString(String date) {
		Date startDate = null;
		try {
			startDate=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MINUTE, intervallo);
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public int getIntervallo() {
		return intervallo;
	}


	public void setIntervallo(int intervallo) {
		this.intervallo = intervallo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public int getFfs() {
		return ffs;
	}

	public void setFfs(int ffs) {
		this.ffs = ffs;
	}

	public int getSpeedlimit() {
		return speedlimit;
	}

	public void setSpeedlimit(int speedlimit) {
		this.speedlimit = speedlimit;
	}

	public int getFrc() {
		return frc;
	}

	public void setFrc(int frc) {
		this.frc = frc;
	}

	public int getNetclass() {
		return netclass;
	}

	public void setNetclass(int netclass) {
		this.netclass = netclass;
	}

	public int getFow() {
		return fow;
	}

	public void setFow(int fow) {
		this.fow = fow;
	}

	public String getRoutenumber() {
		return routenumber;
	}

	public void setRoutenumber(String routenumber) {
		this.routenumber = routenumber;
	}

	public String getAreaname() {
		return areaname;
	}

	public void setAreaname(String areaname) {
		this.areaname = areaname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * restituisce la somma delle velocit calcolate in un certo momento di un certo intervallo
	 * @return la somma delle velocit calcolate in un certo momento di un certo intervallo
	 */
	public double getSomma() {
		return somma;
	}

	/**
	 * setta somma delle velocit calcolate in un certo momento in un certo intervallo
	 * @param somma delle velocit calcolate in un certo momento in un certo intervallo da settare
	 */
	public void setSomma(double somma) {
		this.somma = somma;
	}

	/**
	 * restituisce il numero di sample ricevuto in un certo intervallo di tempo
	 * @return il numero di sample ricevuto in un certo intervallo di tempo
	 */
	public int getCount() {
		return count;
	}

	/**
	 * setta il numero di sample ricevuto in un certo intervallo di tempo
	 * @param count il numero di sample ricevuto in un certo intervallo di tempo
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Funzione che restituisce tutti i valori in una stringa utile per l'invio con activeMQ
	 * @param avgSpeed la velocit media che si vuole inserire nella stringa
	 * @param avgTravelTime il travelTime medio che si vuole aggiungere nella stringa
	 * @return stringa che rappresenta l'oggetto link
	 */
	public String generateTextBody(float avgSpeed,  float avgTravelTime) {
		String s = "Link [id=" + id + ", length=" + length + ", ffs=" + ffs + ", speedlimit=" + speedlimit + ", frc=" + frc
				+ ", netclass=" + netclass + ", fow=" + fow + ", routenumber=" + routenumber + ", areaname=" + areaname
				+ ", name=" + name + ", geom=" + Arrays.deepToString(geom);

		s = s + ", avgspeed="+ (avgSpeed) + ", avgtraveltime=" +convertHoursInStringDate(avgTravelTime);

		s = s +  "]";

		return s;
	}

	/**
	 * 
	 * Funzione che restituisce tutti i valori all'interno di un oggetto json
	 * @param avgSpeed la velocit media che si vuole inserire nella stringa
	 * @param avgTravelTime il travelTime medio che si vuole aggiungere nella stringa
	 * @return stringa json che rappresenta l'oggetto Link
	 */
	public String generateJsonBody(double avgSpeed,  double avgTravelTime, String timestamp) {
		JSONObject link = new JSONObject();

		link.put("id",id);
		link.put("length",length);
		link.put("ffs",ffs);
		link.put("speedlimit",speedlimit);
		link.put("frc",frc);
		link.put("netclass",netclass);
		link.put("fow",fow);
		link.put("routenumber",routenumber);
		link.put("areaname",areaname);
		link.put("name",name);
		link.put("geom",Arrays.deepToString(geom));
		link.put("timestamp",timestamp);
		link.put("avgspeed",(avgSpeed));
		link.put("avgtraveltime",convertHoursInStringDate(avgTravelTime));
		StringWriter out = new StringWriter();
		try {
			link.writeJSONString(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out.toString();
	}

	/**
	 * 
	 * Funzione che restituisce i valori sintetici all'interno di un oggetto json
	 * @param avgSpeed la velocit media che si vuole inserire nella stringa
	 * @param avgTravelTime il travelTime medio che si vuole aggiungere nella stringa
	 * @return stringa json che rappresenta l'oggetto Link
	 */
	public String generateSyntheticJsonBody(double avgSpeed,  double avgTravelTime, String timestamp) {
		JSONObject link = new JSONObject();

		link.put("id",id);
		link.put("areaname",areaname);
		link.put("timestamp", timestamp);
		link.put("avgspeed",(avgSpeed));
		link.put("avgtraveltime",convertHoursInStringDate(avgTravelTime));


		StringWriter out = new StringWriter();
		try {
			link.writeJSONString(out);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return out.toString();
	}
	/**
	 * Messaggio per generare il message body nel caso in cui si vuole solo considerare il travel time nel body del messaggio
	 * @param avgTravelTime
	 * @param timestamp
	 * @return stringa json che rappresenta l'oggetto Link
	 * @throws ParseException
	 */
	public String generateMessageAvgTravelTime(double avgTravelTime , String timestamp) throws ParseException
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		DateFormat dateFormat2=new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");

		Date endTime=dateFormat.parse(timestamp);
		Calendar cal;
		cal=Calendar.getInstance();

		cal.setTime(endTime);
		cal.add(Calendar.MINUTE, -3);
		Date startTime=cal.getTime();

		String startTimeS=dateFormat.format(startTime);
		String end=dateFormat2.format(endTime);

		String start=dateFormat2.format(startTime);

		TotalVehiclesTravelTimePayload payload=new TotalVehiclesTravelTimePayload("TotalVehiclesTravelTimePayload", avgTravelTime, 0, startTime, endTime, null);
		ArrayList<Payload> list= new ArrayList<>();
		list.add(payload);
		Packet p = new Packet(this.id ,  list);
		return Packet.toJson(p);

		//	JSONObject link=new JSONObject();
		//link.put("edgeId", id);
		//	link.put("payloads", "[{\"travelTime\":"+ convertHoursInStringDate(avgTravelTime)+",\"startTime\":" + startTimeS + ",\"endTime':" + timestamp + "]}");
		//	link.put("'avgtraveltime':"+ convertHoursInStringDate(avgTravelTime)+"'starttime':" + startTimeS + "'endtime':" + timestamp);
		//	link.put("endtime", timestamp););
		//	link.put("starttime", startTimeS);
		//	link.put("endtime", timestamp);

		/*
		 JSONArray list1 = new JSONArray();

		 list1.add("\"travelTime\":" + avgTravelTime);
	        list1.add("\"endTime\":" + end);
	        list1.add("\"startTime\":" + start);
	        link.put("payloads", list1);
		StringWriter out = new StringWriter();
		try {
			link.writeJSONString(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 */
	}
}