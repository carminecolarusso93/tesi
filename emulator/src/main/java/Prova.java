import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.jms.ActiveMQJMSClient;
import org.apache.activemq.artemis.api.jms.JMSFactoryType;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.BasicConfigurator;
import util.SettingReader;

import javax.jms.*;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class Prova {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        SettingReader st = new SettingReader();

        String obsFilePath = st.readElementFromFileXml("settings.xml", "Files", "observations");

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("host", "localhost");
        map.put("port", 61616);
        TransportConfiguration transportConfiguration = new TransportConfiguration(NettyConnectorFactory.class.getName(),map);
        ActiveMQConnectionFactory cf = ActiveMQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.QUEUE_CF,transportConfiguration);

        QueueConnection connection;
        QueueSession session;

        try {
            connection = cf.createQueueConnection();
            session=connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("prova1");
            QueueSender sender;
            sender = session.createSender(queue);
            Message message=session.createTextMessage("ciaotony");
            message.setStringProperty("timeStamp", "bello");
            sender.send(message);
        } catch (JMSException e1) {
            e1.printStackTrace();
        }
    }
}
