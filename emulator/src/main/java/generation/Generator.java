package generation;

import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.jms.ActiveMQJMSClient;
import org.apache.activemq.artemis.api.jms.JMSFactoryType;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory;
import org.apache.activemq.artemis.jms.client.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.BasicConfigurator;
import util.SettingReader;

import javax.jms.*;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import static generation.GeneratorUtils.createAndSend;

/**
 * Generator associates each link to its area and sends each sample to the specific areaNode,
 * according to the link, which generated it.
 * Links.csv is used to associate links and areaNodes, whereas Observations.csv contains the samples.
 * A scale factor is used to change samples generation relative speed; it is measured in fractions of seconds.
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco, Antonio De Iasio
 *
 */
public class Generator {
	private static int scala=10;
	private static HashMap<String, String> associations=new HashMap<>();
	private static HashMap<String, Queue> JMSQueues=new HashMap<>();

	/**
	 * Compilation unit. It creates a connection and a session. It then calls the method associateLink2Area
	 * (QueueSession sesssion)
	 * to associate each link to its area and create a JMSQueue for each of them. Finally, it calls the method
	 * sendMessage (QueueSession session), which sends each sample to its queue.
	 * @param args start up arguments
	 */
	public static void main(String[] args)throws InterruptedException {
		BasicConfigurator.configure();
		SettingReader st = new SettingReader();
		String value = st.readElementFromFileXml("settings.xml", "generator", "scala");
		scala = Integer.parseInt(value);
		System.out.println("scala = " + scala);

		//String url = st.readElementFromFileXml("settings.xml", "areaNode", "urlIn");

		TransportConfiguration transportConfiguration = new TransportConfiguration(NettyConnectorFactory.class.getName());
		ActiveMQConnectionFactory cf = ActiveMQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.QUEUE_CF,transportConfiguration);
		QueueConnection connection;
		QueueSession session = null;
		try {
			connection = cf.createQueueConnection();
			session=connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		} catch (JMSException e1) {
			e1.printStackTrace();
		}
		associateLink2Area(session);
		sendMessage(session);
	}
	
	/**
	 * This method reads Observations.csv line by line. Then according to the areNode where the link, whose vehicle is
	 * transiting on, belongs to, builds a JMS text message and sends it.
	 * The text is sent to the whole string constituing the sample and the following properties are set.
	 * "link": link ID, long
	 * "coverage": link coverage, float
	 * "timestamp": observation timestamp, string
	 * "speed": vehicle avg sped, float
	 * The sending of messages has been temporized: the time interval between two consecutive samples is divided
	 * by a scale factor in the file setting.xml
	 * @param session the session used to create the sender
	 * @throws InterruptedException interruption problem
	 */
	private static void sendMessage(QueueSession session) throws InterruptedException {
		SettingReader st = new SettingReader();
		String obsFilePath = st.readElementFromFileXml("settings.xml", "Files", "observations");
		createAndSend(session, obsFilePath, scala, JMSQueues, associations);
	}

	/**
	 * This method associate a link to an area. The side effect is the creation of an HashMap<String, String></String,>,
	 * where the key will be the link and the value the areName. Moreover a JMSQueue is created for every area.
	 * The queues will be stored in an HashMap<String, JMSQueue>, where the key is the arename and the value is the
	 * associated queue.
	 * @param session session la sessione con la quale si creer� la queue
	 */
	private static void associateLink2Area(QueueSession session) {
		SettingReader st = new SettingReader();
		String linksFilePath = st.readElementFromFileXml("settings.xml", "Files", "links");
		Queue queue=null;
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(linksFilePath));
			CSVFormat csvFormat = CSVFormat.DEFAULT.withFirstRecordAsHeader().withDelimiter(';');
			CSVParser csvParser = csvFormat.parse(reader);
			for (CSVRecord r: csvParser) {
				associations.put(r.get("id"),r.get("areaname"));
				try{
					queue = session.createQueue(r.get("areaname"));
				} catch (JMSException e) {
					System.err.println("ERROR JMS: " + e);
				}
				JMSQueues.put(r.get("areaname"), queue);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}