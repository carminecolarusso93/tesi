package generation;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.BasicConfigurator;
import util.SettingReader;

import javax.jms.*;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashMap;

import static generation.GeneratorUtils.createAndSend;

/**
 * La classe GeneratorThread.java ha il compito di associare il link alla giusta area e di inviare
 * i sample al corretto nodo area in base al link specificato nel sample. Per l'associazione
 * tra link ed area si sfruttano le informazioni presenti all'interno del file "Links.csv" o di un suo sottoinsieme mentre il 
 * file contenete i samples � "Observation.csv" o di un suo sottoinsieme.
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public class GeneratorThread extends Thread{
	//si utilizza un fattore di scala per l'invio del messaggio. Si considera 
	//il valore qui presente � solo indicativo, dato che verr� poi sostituito dal valore presente nel file setting.xml
	private int scala=10;
	private String url;
	private  HashMap<String, String> associazioni=new HashMap<>();
	private HashMap<String, Queue> JMSQueues=new HashMap<>();
	private  String linkFilePath;
	private  String obsFilePath;

	/**
	 * Costruttore del thread a cui si passano due valori:
	 * @param linkFile il nome del file contenente le strade e le aree su cui il thread dovr� lavorare.
	 * @param obsFile il nome del file contenente i sample riferenti solo alle sue aree e strade.
	 */
	public GeneratorThread(String linkFile, String obsFile) {
		this.linkFilePath=linkFile;
		this.obsFilePath=obsFile;
	}
	
	/**
	 * Il metodo run contiene le operazioni eseguite dal thread:
	 * Legge la scala da voler utilizzare e crea una sessione con la quale creer� tante code quante sono le aree presenti nel suo 
	 * file linkFile.
	 * Esegue il riempimento delle code in base a i sample letti nel file con nome presente nella variabile obsFile
	 */
	public void run() {
		SettingReader st = new SettingReader();
		String value = st.readElementFromFileXml("settings.xml", "generator", "scala");
		scala = Integer.parseInt(value);
		System.out.println("scala = " + scala);
		url = st.readElementFromFileXml("settings.xml", "areaNode", "urlIn");
		QueueConnectionFactory factory=new ActiveMQConnectionFactory(url);
		QueueConnection connection = null;
		QueueSession session = null;
		try {
			connection = factory.createQueueConnection();
			session=connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		} catch (JMSException e1) {
			e1.printStackTrace();
		}
		associaLinkArea(session);
		try {
			sendMessage(session);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Questo metodo legge una linea dal file contenete i sample (ogni linea del file � un sample),
	 * determina la giusta queue alla quale inviare il sample in base all'area cui appartiene il link sul quale il veicolo transita,
	 * costruisce il messaggio JMS e lo invia. Il messaggio JMS � un TextMessage. Si setta come testo del messaggio l'intera stringa costituente il sample
	 * e si definiscono le seguenti properties:
	 * "link": un long che rappresenta l'id del link
	 * "coverage": un float che indica la copertura della strada
	 * "timestamp": una stringa che rappresenta il tempo in cui � stata effettuata l'osservazione
	 * "speed": un float che rappresenta la velocit�
	 * L'invio dei messaggi � temporizzato. Si considera l'intervallo di tempo che intercorre tra due samples successivi
	 * e si divide per una fattore di scala modificabile nel file setting.xml. 
	 * @param session session la sessione con la quale verr� creato il sender
	 * @throws ParseException 
	 * @throws InterruptedException
	 * @throws JMSException
	 */
	private void sendMessage(QueueSession session) throws InterruptedException {
        createAndSend(session, obsFilePath, scala, JMSQueues, associazioni);
    }

	/**
	 * Questo metodo effettua l'associazione tra link ed area. Si ottiene una hashMap con chiave di tipo Stringa e valore di tipo Stringa in cui
	 * la chiave sar� il linkId ed il valore l'areaName. Inoltre crea una JMSQueue per ogni area. Si ottiene una HashMap la cui chiave di tipo stringa 
	 * rappresenta il nome dell'area ed il cui valore di tipo JMSQueue rappresenta la coda associata.
	 * @param session session la sessione con la quale si creer� la queue
	 * @throws JMSException
	 */
	private void associaLinkArea(QueueSession session) {
		Queue queue=null;
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(linkFilePath));
			CSVFormat csvFormat = CSVFormat.DEFAULT.withFirstRecordAsHeader().withDelimiter(';');
			CSVParser csvParser = csvFormat.parse(reader);
			for (CSVRecord r: csvParser) {
				associazioni.put(r.get("id"),r.get("areaname"));
				try{
					queue = session.createQueue(r.get("areaname"));
				} catch (JMSException e) {
					System.err.println("ERRORE JMS: " + e);
				}
				JMSQueues.put(r.get("areaname"), queue);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Main il cui compito eseguire un numero di thread par al valore presente in setting.xml.
	 * Ad ogni thread passa due stringhe che rappresentano i nomi del file da leggere.
	 * I nomi partono da 0 a numero di thread-1 e devono essere inseriti in due cartelle, link e obs. 
	 * @param args
	 */
	public static void main(String[] args) {
		BasicConfigurator.configure();
		SettingReader st = new SettingReader();
		// righe di codice che servono per cambiare la variabile scala in base al valore presente nel file xml "settings.xml"
		String value = st.readElementFromFileXml("settings.xml", "generator", "numberOfThreads");
		int threads = Integer.parseInt(value);
		System.out.println("threads = " + threads);
		GeneratorThread[] generators= new GeneratorThread[threads];
		for(int i=0; i<threads;i++)
			generators[i]=new GeneratorThread("links/"+i+".csv","obs/"+i+".csv");
		for(int i=0; i<threads;i++)
			generators[i].start();
	}
}
