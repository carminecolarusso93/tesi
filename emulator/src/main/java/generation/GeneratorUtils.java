package generation;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import javax.jms.*;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class GeneratorUtils {
    /**
     * Questo metodo serve a calcolare la differenza tra i tempi di ricezione dei due sample. Il primo parametro
     * indica il tempo del primo campione, il secondo indica il tempo di ricezione del campione successivo
     * @param d1 tempo del primo campione
     * @param d2 tempo del secondo campione
     * @return long millisDiff/scala differenza dei tempi dei due campioni in millisecondi diviso la scala specificata in settings.xml
     * @throws ParseException
     */
    public static long diffTempi(String d1, String d2, int scala) {
        SimpleDateFormat fmt=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date1 = null;
        try {
            date1 = fmt.parse(d1);
        } catch (ParseException e) {
            System.err.println("ERRORE PARSING DATA: " + e);
        }
        Date date2 = null;
        try {
            date2 = fmt.parse(d2);
        } catch (ParseException e) {
            System.err.println("ERRORE PARSING DATA: " + e);
        }

        long millisDiff=date2.getTime() - date1.getTime();
        return millisDiff/scala;
    }

    public static void createAndSend(QueueSession session, String obsFilePath, int scala, HashMap<String, Queue> jmsQueues, HashMap<String, String> associations) throws InterruptedException {
        String previousTmp = null;
        String currentTmp;
        Reader reader;
        try {
            reader = Files.newBufferedReader(Paths.get(obsFilePath));
            CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';');
            CSVParser csvParser = csvFormat.parse(reader);
            for (CSVRecord r: csvParser){
                currentTmp=r.get(3);
                if(r.getRecordNumber() == 1)
                    previousTmp = currentTmp;
                long millisDiff = diffTempi(previousTmp, currentTmp, scala);
                Thread.sleep(millisDiff);
                previousTmp = currentTmp;
                Queue queueLink = jmsQueues.get(associations.get(r.get(1)));
                QueueSender sender;
                try {
                    sender = session.createSender(queueLink);
                    //TODO perhaps roll it back to the way the guys created it
                    Message message=session.createTextMessage(r.toString());
                    message.setLongProperty("link", Long.parseLong(r.get(1)));
                    message.setFloatProperty("coverage", Float.parseFloat(r.get(2).replace(',','.')));
                    message.setStringProperty("timeStamp", r.get(3));
                    message.setFloatProperty("speed", Float.parseFloat(r.get(4)));
                    sender.send(message);
                } catch (JMSException e) {
                    System.err.println("ERRORE JMS: " + e);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}