package node;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import link.Link;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.BasicConfigurator;
import util.SettingReader;


/**
 * Classe utilizzata per l'inizializzazione dei vari AreaNode. Il processo parte dalla lettura da file delle informazioni
 * dei singoli link. Da questi si recuperano il numero di aree e di conseguenza si instanziano i diversi AreaNode, contestualmente vengono generati
 * tutti i link.
 * 
 * @author De Luca Lucio, Grimaldi Gaia, Tedesco Francesco
 *
 */
public class Initialization {

	/**
	 * 
	 * Enum utilizzato per identificare i due tipi di nodo sensore realizzabili.
	 * AVGSPEEDONLINK identifica il sensore che calcola la velocit� media su un link in un intervallo di tempo
	 * AVGSPEEDVEHICLEONLINK identifica il sensore che calcola la velocit� media di un veicolo su un link
	 *
	 */
	private enum SensorType {
		AVGSPEEDONLINK,
		AVGSPEEDVEHICLEONLINK
	}

	private static String linkFilePath="Links.csv";

	
	public static void main(String[] args) {
		BasicConfigurator.configure();
		SensorType sensorType;
		SettingReader st = new SettingReader();
		String value = st.readElementFromFileXml("settings.xml", "areaNode", "sensorType");
		System.out.println(value);		
		if (Integer.parseInt(value) == 0 )
			sensorType = SensorType.AVGSPEEDONLINK;
		else
			sensorType = SensorType.AVGSPEEDVEHICLEONLINK;
		System.out.println(sensorType);

		boolean boolMultipleNorthBoundQueues;
		value = st.readElementFromFileXml("settings.xml", "areaNode", "multipleNorthBoundQueues");
		boolMultipleNorthBoundQueues = Integer.parseInt(value) == 1 ;

		String urlIn = st.readElementFromFileXml("settings.xml", "areaNode", "urlIn");
		String urlOut = st.readElementFromFileXml("settings.xml", "areaNode", "urlOut");
		
		String interval = st.readElementFromFileXml("settings.xml", "Link", "intervallo");
		String startTime = st.readElementFromFileXml("settings.xml", "Link", "startTime");

		value = st.readElementFromFileXml("settings.xml", "Link", "msgType");
		int msgType = Integer.parseInt(value) ;
		
		linkFilePath = st.readElementFromFileXml("settings.xml", "Files", "links");
		System.out.println(linkFilePath);
		
		HashMap<String, AreaNode> areas = new HashMap<>();

		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(linkFilePath));
			CSVFormat csvFormat = CSVFormat.DEFAULT.withFirstRecordAsHeader().withDelimiter(';');
			CSVParser csvParser = csvFormat.parse(reader);
			for (CSVRecord r: csvParser){
				AreaNode an = null;
				Link link = new Link(Long.parseLong(r.get("id")), Float.parseFloat(r.get("length").replace(',','.')),
						Integer.parseInt(r.get("ffs")), Integer.parseInt(r.get("speedlimit")),Integer.parseInt(r.get("frc")),
						Integer.parseInt(r.get("netclass")),Integer.parseInt(r.get("fow")),
						r.get("routenumber"),r.get("areaname"),r.get("name"),r.get("geom"),
						Integer.parseInt(interval),
						startTime,msgType);
				if(areas.containsKey(r.get("areaname")))
					areas.get(r.get("areaname")).addLink(link);
				else {
					if (sensorType == SensorType.AVGSPEEDONLINK)
						an = new AvgSpeedLink(urlIn, urlOut, r.get("areaname"), link, boolMultipleNorthBoundQueues);
					else if (sensorType == SensorType.AVGSPEEDVEHICLEONLINK)
						an = new AvgSpeedVehicleLink(urlIn, urlOut, r.get("areaname"), link, boolMultipleNorthBoundQueues);
					areas.put(r.get("areaname"), an);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Initialization completed...");
		//utilizzo un busy waiting per tenere attive le varie istanze di AreaNode create
		while (true) { }
	}
}